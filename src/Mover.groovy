/**
 * Script para mover arquivos para cada pasta especifica de acordo com um arquivo a ser consumido
 * Created by jefferson on 13/09/16.
 */
def arquivoDeLeitura = "/home/jefferson/Área de Trabalho/doc.txt"
def caminhoPrincipal = "/home/jefferson/exemplo/"

new File(arquivoDeLeitura).eachLine {linha ->

    def partes = linha.split(";") //Quebra de cada linha padrão: Pasta;NomeArquivo.extensão
    def pastaDestino = caminhoPrincipal+partes[0].trim() //Caminho absoluto da pasta a ser criada/usada
    def arquivo = caminhoPrincipal+partes[1].trim() //Caminho absoluto do arquivo antes de ser movido

    if(existePasta(pastaDestino)){
        if(existeArquivo(arquivo)){
            moverArquivos(arquivo,pastaDestino)
            println("O DIRETÓRIO: "+pastaDestino+" existe, e o arquivo "+partes[1].trim()+ " foi movido com SUCESSO!")
        }else{
            println("O DIRETÓRIO: "+pastaDestino+" existe, mas o arquivo "+partes[1].trim()+ " NÃO, portanto o mesmo NÃO foi MOVIDO!")
        }
    }else{
        new File(pastaDestino).mkdirs()
        if(existeArquivo(arquivo)){
            moverArquivos(arquivo,pastaDestino)
            println("O DIRETÓRIO: "+pastaDestino+" foi criado, e o arquivo "+partes[1].trim()+ " foi movido com SUCESSO!")
        }else{
            println("O DIRETÓRIO: "+pastaDestino+" foi criado, mas o arquivo "+partes[1].trim()+ " NÃO existe, portanto o mesmo NÃO foi MOVIDO!")
        }
    }
}
/*
* Método responsável por Verificar se a pasta existe
*/
def existePasta(diretorio){
    def nomePasta = new File(diretorio)
    if(nomePasta.exists())
        return true
    else
        return false
}
/*
* Método responsável por Verificar se o arquivo existe
*/
def existeArquivo(diretorio){
    def nomeArquivo = new File(diretorio)
    if(nomeArquivo.exists())
        return true
    else
        return false
}
/*
* Método Responsável por Mover os arquivos
*/
def moverArquivos(arquivo,diretorio){
    def file = new File(arquivo)
    def dir = new File(diretorio)
    boolean moverArquivo = file.renameTo(new File(dir,file.getName()))

    return moverArquivo
}
